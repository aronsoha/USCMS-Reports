\clearpage
# Detector Operations

The LHC's Long Shutdown 2 (LS2) continued this quarter. Major activities
for the US include the upgrade of front end electronics in part of the
CSC system. The "Phase 1" upgrade of the hadronic barrel calorimeter was
completed during this quarter. All subsystems are taking advantage of
the opportunity to perform maintenance tasks. A new schedule for the LHC
has been announced, with LS2 being extended by two months (the LHC will
turn back on May 2021) and the delay of LS3 by one year (it will now
begin in 2025). This new schedule may allow for the installation of
improved cooling circuits on ME-1/1 CSC chambers.

## BRIL 

During LS2 two PLT detectors are being built for the BRIL system. Two
laboratories (P5, TIF) were established, for assembly and testing of the
new PLTs, and for studying the radiation damage on the old extracted
PLT. Two quadrants for the first replacement PLT have been assembled and
tested with the source. The first one was re-assembled after thermal
paste became available (after a world-wide shortage). Both quadrant
readouts tested successfully and they are mounted on temporary carriages
and stored in freezer boxes. A new freezer is processed to serve for the
cold cycle tests - dry air plant is ready; interlock system is under
construction. The re-designed opto-motherboards (13) were produced at
CERN; 7 tested with a faulty ALT chip, 3 with more than one faulty chip.
Four boards are under repair at Rutgers. More pixel ROC planes are
received and undergo source testing. The patch-panel PPS0 has to be
rebuilt with modifications; the 10 needed PCB boards are produced and
the equipment list was finalized after a prototype testing; now moved to
assembly. The full mechanical assembly is progressing - cooling pipes
were all successfully subjected to leak and pressure tests. We expect to
finish the first PLT quadrants in February and move to cold-cycle
testing (for the previous PLT two telescopes developed faults after
several thermal cycles).

The operation software update and documentation continues. Systematic
studies of radiation damage with the Run-2 PLT performed by graduate
students. The software for luminosity calculation for the collaboration
is finalized and continuous feedback to the collaboration is provided
via hypernews.

  -----------------------------------------------------------
  Subsystem   Description                Scheduled   Achieved
  ----------- ------------------------ ----------- ----------
  BRIL        Assembly of the          February 10 
              replacement PLT - 3rd                
              quarter                              

  BRIL        Source tests 3rd quarter      Feb 31 

  BRIL        Commission cold box           Feb 31 

  BRIL        Assemble quarter 4          March 15 

  BRIL        Certify quarter 4 with      March 31 
              source                               

  BRIL        Final mechanical            April 31 
              assembly of PLT 1                    

  BRIL        Assemble quadrants PLT 2     June 30 

  BRIL        Finalize first round         July 31 
              thermal cycle tests PLT              
              1 (start PLT 2)                      

  BRIL        Finalize installation         Dec 31 
              procedure PLT 1                      
  -----------------------------------------------------------

  : BRIL 2020 Upgrade Milestones

  -----------------------------------------------------------
  Subsystem   Description                Scheduled   Achieved
  ----------- ------------------------ ----------- ----------
  BRIL        Assembly of the               Jun 30  August 25
              replacement PLT - first              
              quarter                              

  BRIL        Source tests done - one       Oct 15      Nov 1
              quarter                              

  BRIL        Assemble quarters 2           Nov 31     Nov 31

  BRIL        Certify 2 quarters with       Dec 15     Dec 15
              source                               

  BRIL        Mechanical holder for         Dec 31     Dec 31
              PLT quarters ready                   

  BRIL        Full Run 2 Luminosity         Dec 31     Dec 10
              analysis /software                   
              review                               
  -----------------------------------------------------------

  : BRIL 2019 Milestones

## Tracker 

Despite a major power cut, planned work on the tracker detector occurred
mostly as scheduled this quarter. Some DAQ recovery and environmental
monitoring system repairs were performed for the strip tracker. Multiple
pixel maintenance efforts proceeded in parallel. No significant new
problems were encountered.

### Pixels 

LS2 projects made good progress this quarter. On the powering front, the
refurbishment of the module power supplies is nearly complete, with
installation of the refurbished units beginning in December and
continuing through January. Production of new DC-DC converters is a bit
behind the initial schedule due to (now resolved) problems with the coil
RF shields, but even with these delays, over two months' contingency
remains in the schedule. Furthermore, the quality of the new converters
is very high.

A solution to the problem of broken FPIX cooling connections at the half
cylinder end flange has now been found and endorsed by the community.
Parts are currently being machined and pressure-tested before welding to
the end flange. Additional strain relief to reduce the likelihood of a
breakage in subsequent connections/disconnections has been designed and
tested, and will be produced and sent to CERN shortly.

A safety switch has been added to the water glycol chiller in the SX5 RP
area so that power to the chiller electronics is decoupled from power to
the circulation system. Improvements to the SX5 clean room piquet
documentation have been made in advance of the CERN Christmas lab
closure.

Finally, many DAQ improvements are ongoing and/or are ready to be tested
in miniDAQ followed by the next MWGR. These include upgrades to all of
the $\mu$TCA XDAQ supervisors and monitors and implementation of a
faster calibration procedure taking advantage of the $\mu$TCA crate
architecture.

### Strips

The quarter began with DAQ maintenance and repair work. In October, some
broken power supplies and a broken FED were successfully replaced. Two
control rings that were potentially damaged by an S1 water leak incident
in June were investigated. One ring was recovered, but the other is
still under investigation. Further debugging was prevented by an
inadvertent triggering of the water mist system in the CMS control room.
Instead, work shifted to interventions on the environmental monitoring
system.

In November, the tracker was warmed up to allow access to the bulkhead
for beampipe support measurements. In parallel, some work was done both
at the bulkhead and in the gas room to improve environmental monitoring,
including the re-connection of an Arduino-monitored humidity sensor,
exchange of oil in a few Vaisala humidity sensors, and exchange of a dry
gas flowmeter. The humidity seal was re-closed at the end of the month
for testing the strips at -25$^{\circ}$C in December, but this work has
been delayed to 2020 due to a major unexpected CERN-wide power cut,
problems with the latest TCDS upgrade, P5 network interventions, and an
announced delay of the next CMS MWGR to 2020.

The strip detector remained off at 0$^{\circ}$C in December.

  Subsystem   Description                         Scheduled   Achieved
  ----------- --------------------------------- ----------- ----------
  Tracker     Pixel Phase 1 Detector Removed         Jan 31     Jan 15
  Tracker     Strips cold and in global run          June 1     Mar 20
  Tracker     Strips tested at -25C                  Dec 11 
  Tracker     Pixel initial checkout complete        Aug 31      Aug 7

  : Tracker 2019 Milestones


  Subsystem   Description                                 Scheduled   Achieved
  ----------- ----------------------------------------- ----------- ----------
  Tracker     Pixel Phase 1 Detector Repairs complete        Jun 30 
  Tracker     Strips cold and in global run                  June 1 
  Tracker     Strips tested at -25C                          Jun 31 
  Tracker     Repaired Pixel checkout complete               Aug 31 
  Tracker     Pixel ready for installation                   Oct 31 

  : Tracker 2020 Milestones


## ECAL 

The deep maintenance of the two lasers was completed on schedule. This
involved replacement of the pump diodes, thermal sensors, water cooling
fittings and various routine maintenance tasks. The lasers were
recommissioned and are now fully operational. The refurbishment of the
new laser barracks involving civil construction of a new floor and roof
supports is ongoing. The milestone for completion is pushed back to July
2020. This does not interfere with laser operations as the old barracks
will be used until the new one is ready.

A new problem was found with the ECAL low voltage MARATON supplies. In
2015 the front panel and elbow brass water fittings were replaced with
stainless steel fittngs due to corrosion of the nickel plated surface
which led to ruptures and water leaks. In 2015 the straight fittings
showed no sign of degradation and were not replaced. Inspection of ECAL
MARATONs now show signs of corrosion which is confirmed in the other LHC
experiments that use MARATONs. These will need to be replaced for all
136 MARATONs. The work will take place in 2020. A new milestone has been
added for this

The detector control system (DCS) upgrade has been completed and
successfully recommisioned. The upgrade of the cooling infrastructure in
LS2 in preparation for LS3 has been pushed back to 2020 by CERN
technical co-ordination and the milestones moved accordingly.

  -----------------------------------------------------------
  Subsystem   Description                Scheduled   Achieved
  ----------- ------------------------ ----------- ----------
  ECAL        Complete First Laser          Sep 30     Sep 30
              Maintenance               (April 30) 

  ECAL        Complete Second Laser         Dec 31     Dec 31
              Maintenance                (July 31) 

  ECAL        Complete replacement of      June 30    April 1
              all Maraton input/output             
              Cooling Pipes                        

  ECAL        Complete replacement of      Sept 30     May 15
              input HV power                       
              connectors                           

  ECAL        Complete modification of      Oct 31    April 1
              CANBUS and update DCS                
              software                             

  ECAL        Complete VME PC               Mar 31   March 15
              replacement                          

  ECAL        Complete network upgrade      May 31     May 31
  -----------------------------------------------------------

  : ECAL 2019 Milestones

  -----------------------------------------------------------
  Subsystem   Description                Scheduled   Achieved
  ----------- ------------------------ ----------- ----------
  ECAL        Complete infrastructure      July 31 
              update of laser barracks             

  ECAL        Complete replacement of        Dec 1 
              all MARATON brass                    
              Cooling fittings                     

  ECAL        Complete upgrade to           June 1 
              XDAQ15                               

  ECAL        Complete installation of      Aug 31 
              new water pump at                    
              surface                              

  ECAL        Complete insulation of        Dec 31 
              surface to underground               
              area pipes                           
  -----------------------------------------------------------

  : ECAL 2020 Milestones


## HCAL

During the fourth quarter of 2019, the HCAL Operations group focused on
completing the HCAL Barrel (HB) Phase 1 Upgrade electronics installation
and commissioning. The HB Phase 1 upgrade, planned for LS2, was
completed ahead of schedule on Oct. 7th. All 36 upgrade HB readout boxes
(RBXes) have been installed in the detector and have been commissioned.
This includes RBX HBP17, which required its backplane to be replaced.
Health checks revealed 8 dead channels in HBP03, which required a bad
VTTx to be fixed in November.

The Co-60 sourcing campaign was completed at the end of October. The
goals of the Co-60 sourcing is to check and validate the quality of the
connections of the megatile cables to the readout modules and to provide
a start-up calibration of the upgraded HB.

The new solid state laser that arrived at CERN last quarter was
commissioned and the signal distribution was checked on the HB. The
laser to SiPM is ok in every RBX in the HB (after fiber swaps were
fixed), but the laser to megatile links for HBP11, HBP12, and HBP13 are
broken. Access to the HBP to evaluate and (if possible) repair the
fiber-optic cables is not possible until March 2020. In November, the
laser stopped functioning and was sent back to the US manufacturer for
repair. The goal is to have it repaired, re-installed, and commissioned
by April 2020.

The HEM15, and HEM16 modules that were damaged by the low voltage spikes
during the 2018 run have been repaired. The megatile cables for the two
RBXes were connected on Nov. 16th and the systems commissioned. The data
communication link with the HEP06 went down on November 9th.
Investigations of the HEP06, as well as HEM09 and HEP10, which had
communication issues during the 2018 run, are ongoing at B904. The
control modules in these RBXes will be replaced, installed, and
commissioned by September 2020.

The issue with the CAEN low voltage power supplies (PS) occasionally
producing voltage spikes when powered on is being addressed. An
overvoltage protection circuit has been developed which clamps the
output of the power supply if voltage exceeds 12V and forces the PS to
trip. A prototype protection board has successfully tested on CAEN power
supplies. The new goal is to have these installed and tested by March
2020. Discussions with CAEN about proper modifications to the PS are
ongoing and will also be completed by April 2020.

Participation of the full HCAL (HB, HE, HF, HO) in the MWGR at the end
of November was anticipated before the MWGR was cancelled. The HCAL has
spent much of the last six weeks of the quarter turned off because of
power cuts, the unavailability of the CMS TCDS which is needed for slow
controls, and eventually the holidays.

Two non-vital tasks have been added to the list of 2020 milestones to
improve operations. The HF control links are still running at 5 Gbits/s
while the HB/HE is running at a more robust 2.5 Gbits/s. It would be
nice to match the control link protocol in the HF to that of the HB/HE
by September 2020. Also, the Acromag (slow-control) crate for the HO
detector should be replaced/upgraded to ensure better running stability
by October 2020.

  ---------------------------------------------------------------------------
  Subsystem   Description                             Scheduled      Achieved
  ----------- ----------------------------------- ------------- -------------
  HCAL        HBE assembly starts at CERN            1-Sep-2018    1-Sep-2018

  HCAL        HBE production in "factory mode"      15-Nov-2018   20-Nov-2018

  HCAL        HBE IRR passed                        15-Feb-2019   15-Feb-2019

  HCAL        HBE Minus end upgrade installation    19-Feb-2019   18-Feb-2019
              begins                                            

  HCAL        HBE production complete               28-Feb-2019  30-July 2019

  HCAL        HBE Minus end Upgrade Complete        30-Jun-2019   15-Sep-2019

  HCAL        HBE Plus end upgrade installation      1-Sep-2019    1-Mar-2019
              begins                                            

  HCAL        HBE Plus end Upgrade Complete         20-Dec-2019    7-Oct-2019
  ---------------------------------------------------------------------------

  : HB Upgrade Milestones

  ---------------------------------------------------------------------------
  Subsystem   Description                             Scheduled      Achieved
  ----------- ----------------------------------- ------------- -------------
  HCAL        extract HEM09 and HEP10 CCMs          10-Feb-2019   30-Jan-2019

  HCAL        extract HEM15 and HEM16 CCMs, RMs,    15-Mar-2019   30-Jan-2019
              CUs                                               

  HCAL        finalize half-speed FEC and CCM        1-Apr-2019   15-Mar-2019
              firmware                                          

  HCAL        reinstall HEM09, HEP10 modules         1-Jun-2019      see text

  HCAL        reinstall HEM15, HEM16 modules         1-Jun-2019   16-Nov-2019

  HCAL        recommission HE with half-speed        1-Jun-2019   15-May-2019
              control links                                     

  HCAL        LV PS back to CAEN for modification    1-Jun-2019      see text
              starts                                            

  HCAL        new LV protection circuits             1-Sep-2019      see text
              installed                                         
  ---------------------------------------------------------------------------

  : 2019 HCAL Operations Milestones


  ------------------------------------------------------------------------
  Subsystem   Description                             Scheduled   Achieved
  ----------- ----------------------------------- ------------- ----------
  HCAL        New LV protection circuits            31-Mar-2020   see text
              installed and tested                              

  HCAL        HB laser-to-megatile fiber-optic      30-Apr-2020   see text
              cables complete                                   

  HCAL        New solid state laser re-installed    30-Apr-2020   see text
              and commissioned                                  

  HCAL        Recommended manufacturer              31-May-2020   see text
              modifications to CAEN LV power                    
              supplies complete                                 

  HCAL        All remaining HB and HE control       30-Sep-2020   see text
              modules installed and commissioned                

  HCAL        Control link protocol upgraded to     30-Sep-2020   see text
              match HB/HE (HF)                                  

  HCAL        Slow-control crate for HO detector    31-Oct-2020   see text
              replaced/upgraded                                 
  ------------------------------------------------------------------------

  : 2020 HCAL Operations Milestones


## EMU 

The major CSC activity in Long Shutdown 2 continues to be the
refurbishment with updated electronics of 180 chambers in the inner
rings of the CSC system. Chambers are extracted from CMS and brought to
the surface at SX5 where the older electronics boards are stripped off,
new ones installed. The refurbished chambers are subjected to a battery
of tests in electronics tests stand in SX5, and then monitored under
power on a long-term-test stand. After a leak test of the cooling
circuits, the chambers are reinstalled in CMS and await recommissioning.
The work of chamber refurbishment and testing, infrastructure
installation and re-commissioning is distributed among small teams that
are composed mostly of graduate students and are led by senior
scientists or postdocs.

Three rings on the positive side of the detector, ME+2/1, ME+3/1, and
ME+4/1 (54 chambers in total) had been extracted in the previous quarter
and refurbished. During FY20Q1, the re-installation and commissioning of
these chambers in situ was completed. A long-standing issue was solved
with the procedure for initialization of the electronics after powering
up. After this was fixed, it was no longer necessary to move clock
control boards from one crate to another during commissioning, and the
procedure became simpler and faster.

The extraction of the 36 chambers of the ME+1/1 ring (the inner ring of
the first station on the positive YE+1 end cap disk) began in late
October and was completed on 22 November. As the electronic boards on
the chambers were replaced, the cooling circuits were also replaced with
new jointless copper pipes that were designed to eliminate the risk of
leaks and were manufactured at the CERN main workshop. By the time CERN
shut down for the end-of-year break, half of the ME+1/1 chambers had
been re-installed in CMS and two had been fully commissioned.

The ME-1/1 ring was already refurbished and commissioned before the new
cooling circuits were available, and the original plan was to replace
these circuits during LS3. Since the ME+1/1 work is ahead of schedule
and the duration of LS2 has been extended by two months, it was decided
to extract ME-1/1 chambers for cooling circuit replacement as the LS2
schedule permits to eliminate the need for doing this in LS3. Six ME-1/1
chambers were brought to the surface so far, and more are planned for
January. The remaining ME-1/1 chambers will be extracted and reinstalled
later in 2020 or earlier in 2021, after the re-installation of the beam
pipe.

The final ALCT boards needed for refurbishment were delivered to CERN in
December.

The new low voltage infrastructure was completed in October for the
positive endcap, and work continued on the low voltage for ME-2/1,
ME-3/1, ME-4/1. The old rack heat exchangers for the low voltage system
were also replaced with new ones.

Major progress was made on the optical trigger motherboards (OTMBs) this
quarter. All of the major components were procured, aside from some
connectors that should be delivered in February. The production of pcbs
was launched for both the mezzanine cards and the base boards, and a
sample of five assembled production cards was delivered to Texas A&M in
late December. These five boards are expected to be tested in January,
and if the tests are positive the full production will be released. The
full production quantity is 108 complete boards (base board plus
mezzanine), plus spares. The original schedule called for the OTMBs to
be available for commissioning of the ME+2/1, ME+3/1, ME+4/1 rings, but
months of delays in financial approvals required the implementation of
an alternate strategy where the old OTMBs from ME+1/1 are temporarily
installed in the other crates during commissioning.

In summary, the installation and commissioning of new electronics on CSC
chambers has been completed on four rings (out of eight total) and the
refurbishment of the fifth ring is approximately half way completed. The
work is now about three weeks ahead of the schedule set out in 2018. The
OTMB, the last electronics board needed in LS2, entered the production
phase in December.

  ------------------------------------------------------------------------
  Subsystem   Description                           Scheduled Achieved
  ----------- ----------------------------------- ----------- ------------
  EMU-MEX/1   CSC LV junction boxes ready for          Jan 15 Feb 1
              installation                                    

  EMU-MEX/1   First ME-1/1 chamber extracted to        Feb 25 Feb 25
              SX5                                             

  EMU-MEX/1   CSC DCFEBv2 boards ready for             Mar 11 Mar 7
              installation on ME-1/1                          

  EMU-MEX/1   ALCT-LX100 ready for installation        Mar 11 Mar 7
              in ME-1/1                                       

  EMU-MEX/1   Full production of ALCT-LX150T           Apr 15 Jun 12
              released                                        

  EMU-MEX/1   CSC on-chamber optical fibers ready      Apr 15 May 8
              for installation                                

  EMU-MEX/1   ME-1/1 installed and commissioned         Jun 2 Aug 15

  EMU-MEX/1   ALCT-LX150T ready for installation       Jun 10 Jul 17
              in ME+234/1                                     

  EMU-MEX/1   First ME+234/1 chamber extracted to      Jun 17 Jun 20
              SX5                                             

  EMU-MEX/1   ALCT-LX100 ready for installation        Jun 30 May 17
              on ME2/2,3/2                                    

  EMU-OPS/1   LV power in place for plus end cap        Aug 8 Oct 20

  EMU-OPS/1   Ready to join MWGR with ME-1/1            Sep 1 Jul 18

  EMU-MEX/1   First ME+1/1 chamber extracted to        Oct 28 Oct 28
              SX5                                             

  EMU-OPS/1   Ready to join MWGR with ME+234/1          Nov 1 Nov 11

  EMU-MEX/1   OTMB production complete                 Nov 15 see text
  ------------------------------------------------------------------------

  : EMU 2019 Milestones


  ------------------------------------------------------------------------
  Subsystem   Description                           Scheduled Achieved
  ----------- ----------------------------------- ----------- ------------
  EMU-MEX/1   OTMBs delivered to CERN for first         Mar 5 
              endcap                                          

  EMU-MEX/1   OTMB production complete                 Mar 10 

  EMU-MEX/1   20 ME-1/1 with new cooling circuits      Mar 13 
              installed and commissioned                      

  EMU-MEX/1   First ME-234/1 chamber extracted to      Mar 16 
              SX5                                             

  EMU-OPS/1   LV power in place for minus end cap      Mar 16 

  EMU-MEX/1   ME+1/1 installed and commissioned         Apr 1 

  EMU-OPS/1   Ready to join MWGR with ME+1/1           Apr 10 

  EMU-MEX/1   OTMBs delivered to CERN for second       Apr 14 
              endcap                                          

  EMU-MEX/1   ME-234/1 installed and commissioned      Aug 15 

  EMU-OPS/1   Ready to join MWGR with ME-234/1         Nov 15 
  ------------------------------------------------------------------------

  : EMU 2020 Milestones


## DAQ

The first major procurement of new computer equipment for run 3 has been
done during this quarter. New computer blades have been delivered, which
will be used for the Detector Control System (DCS) and central sys-admin
services. The replacement of the "VMEPC\" machines used as control PC
and to link to VME crates, as microTCA control hub and as general
sub-detector processing nodes has caused a couple of issues. The
candidate machine from the vendor who won the bidding process has an
inferior PCI read performance when using PCI gen-1 cards to communicate
with the VME and compact-PCI crates. Extensive investigation and test
were done to confirm that the performance is sufficient for all our use
cases. However, we still face an issue with the Linco card used by the
drift-tube detector. This problem is being looked into by the vendor.
Unfortunately, the vendor cannot procure the CPUs in time, which results
in a 2-months delay in delivery. This necessities the rescheduling of
the VMEPC replacement at pt.5, which was planned with the sub-detectors
for early 2020.

There were several unforeseen power and cooling cuts during the
reporting period. In addition, work on DSS fired accidentally the safety
water-mist system on Oct 11. These incidents required a lot of
attention, diverting work from preparations for run-3 and planning and
developments for the HL-LHC system.

The online cloud is running reliably on about 31k physical cores
whenever the power and cooling is available. There are many hardware
issues with the 440 Megware machines, which were bought in 2015 and have
been assigned to the permanent online cloud, i.e. they will no longer be
used for the HLT. There are about 80 machines which have problems with
memory DIMMs. These machines are under warranty until March 24, 2020.
However, the procedure to get these DIMMs replaced requires a lot of
work.

A major upgrade of the TCDS system to more powerful FPGAs has been done.
This allows among other enhancements to deploy new firmware and software
with additional bookkeeping counters. These counters are needed to
replace the functionality of SCAL system, which has to be retired for
run 3. The OMS database schema and procedures are mostly ready to use
this information once it has been validated. Once this has been done,
the legacy tables from WbM/SCAL will no longer be needed by OMS.

The OMS project is improving the infrastructure for the user interface
to ease the integration of sub-system specific pages and widgets. In
addition, we have developed a new scheme on how to account for dead- and
downtimes. This will be tested once the information from TCDS is
available.

The tests for the event-builder hardware to be purchased for run-3 have
progressed. We have started to evaluate a machine based on AMD EPYC
7502P ("Rome"), which could be an interesting alternative to dual socket
Intel CPU based systems. Initial results have been presented at CHEP
conference in November. Good progress has been made in understanding the
Ethernet routers from Juniper. Several features did not work as expected
and have been corrected by the vendor. The new system to monitor the
network switches becomes usable. The evaluation of the Lustre storage
system has been completed and we are in contact with the vendor to
finalize the system configuration. The main unknown is the required
throughput for the heavy-ion runs during run 3.

  ---------------------------------------------------------------
  Subsystem   Description                    Scheduled   Achieved
  ----------- ---------------------------- ----------- ----------
  DAQ         Migrated all DAQ s/w to            Feb 1      Feb 1
              gitlab                                   

  DAQ         RCMS and XDAQ releases from       Mar 15     Mar 15
              gitlab deployed                          

  DAQ         Core XDAQ packages built           Jun 1      Apr 9
              with C++11/14                            

  DAQ         Evaluated storage-manager          Aug 1     Oct 15
              h/w for run 3                            

  DAQ         OMS DB tables fully replace        Oct 1 
              legacy tables from WbM/SCAL              

  DAQ         Central DAQ hardware choices       Nov 1 
              finalized                                
  ---------------------------------------------------------------

  : DAQ 2019 Milestones


  ---------------------------------------------------------------
  Subsystem   Description                    Scheduled   Achieved
  ----------- ---------------------------- ----------- ----------
  DAQ         OMS DB tables fully replace        Feb 1 
              legacy tables from WbM/SCAL              

  DAQ         Central DAQ hardware choices       Mar 1 
              finalized                                

  DAQ         CentOS 8 and XDAQ 16 ready         May 1 
              for deployment at pt.5                   

  DAQ         New run-time logger and            Jul 1 
              downtime accounting                      
              available                                

  DAQ         miniDAQs working with run 3        Sep 1 
              data-to-surface network                  

  DAQ         DAQ3 system commissioned          Nov 15 
  ---------------------------------------------------------------

  : DAQ 2020 Milestones


## Trigger

During this quarter the US groups continued their maintenance,
operations, and development work on the Layer-1 calorimeter (CaloL1)
trigger, endcap muon trigger (EMTF), barrel muon trigger (BMTF), and
global Level-1 trigger systems, as well as on the field operations of
the Trigger Studies Group. These preparations will provide improvements
and reliable running during Run 3 data taking operations.

### Muon Trigger

Two dedicated meetings on GEM-EMTF integration took place in this
quarter, with a focus on finalizing data formats between the systems and
developing a plan of work. Discussion on implementing higher precision
on the CSC position and bend angle using look-up tables for improved
fits to the LCT cathode patterns also took place, and is proposed for
Run 3.

The first electronic integration tests successfully took place between
GE1/1 and EMTF at CERN in December. This included pseudorandom bitstream
(PRBS) tests over optical links between the GE1/1 CTP7 card and the EMTF
MTF7 card, with only very infrequent errors on one link corrected by the
link protocol. A second test involved sending calibration pulses from
the GE1/1 front-end electronics to the EMTF over one optical link, with
the data properly received but needing some timing adjustment. The
optical link protocol between the two systems had been upgraded to the
LpGBT standard at 10 Gbps, which allows more data to be transmitted over
fewer links compared to the 8b10 encoding scheme used during Run 2.

For the EMTF online software, functionality to send an alert when the
clock from TCDS is lost is under implementation.

For the BMTF, the Kalman filter is running as the default trigger in the
MWGRs. A new UCLA graduate student has been trained to maintain both the
emulator and the HLS firmware IP core. Work also started with the
Exotica physics community on the physics requirements of BMTF for Run 3,
and studies of modifying the firmware and the emulator to address the
requirements are underway.

### Layer-1 Calorimeter Trigger

During this quarter, the hardware activities of the Layer-1 Calo group
were focused on swapping the MicroSD cards in the production CTP7 cards
and preparing for the MWGR. The MicroSD cards in all CTP7s were swapped,
and the system was checked and ran succesfully with the Layer-2 Calo
system. The new calibration software for Run 3 is under development, and
it was also succesfully checked also with Phase 2 configuration. More
work is still planned to improve the resolution of the jets and single
electron energy reconstruction.

### Global Trigger

Completed some work to port the L1 Trigger DQM code for the muon
subsystems, the ECAL, and the HCAL for the Phase 2 L1 Trigger.

### Field Operations Group of the Trigger Studies Group

Nothing to report, quiet quarter.

  ----------------------------------------------------------------------
  Subsystem   Description                           Scheduled   Achieved
  ----------- ----------------------------------- ----------- ----------
  TRIG        Architecture for bringing GE1/1          Aug 30     Aug 30
              signals to EMTF specified                       

  TRIG        EMTF online software framework           Aug 30    Sept 30
              extended to include GE1/1                       

  TRIG        Neural network PT assignment             Dec 31     Jun 30
              implementation into EMTF firmware               

  TRIG        Initial algorithm to include GE1/1       Dec 31     Dec 31
              into EMTF                                       

  TRIG        Remove legacy BMTF firmware and          Dec 31     Aug 31
              keep only Kalman filter                         

  TRIG        Modify the Kalman filter algorithm       Dec 31 exp Q2FY20
              to use the upgraded trigger                     
              primitives                                      

  TRIG        Deinstall old RCT hardware              Sept 30    Sept 30

  TRIG        Exchange microSDs in all CTP7 cards     Sept 30     Nov 30

  TRIG        Calo Trig: Fix the cable trays in       Sept 30     Sep 30
              P5                                              

  TRIG        Calo Trig: Initial demonstration of       Jul 1     Dec 31
              pileup mitigation algorithms                    
              involving machine learning using                
              High-Level Synthesis                            

  TRIG        Calo Trig: Performance results of        Dec 31     Dec 31
              pileup mitigation algorithms                    
              involving machine learning                      

  TRIG        Assess needs for any rate                Dec 31     Dec 31
              monitoring software developments                

  TRIG        Update rate monitoring software to       Dec 31     Dec 31
              adapt to evolution in DAQ                       
              infrastructure                                  
  ----------------------------------------------------------------------

  : Trigger 2019 Milestones


  ----------------------------------------------------------------------
  Subsystem   Description                           Scheduled   Achieved
  ----------- ----------------------------------- ----------- ----------
  TRIG        GE1/1 signals integrated in EMTF         Dec 31 
              DAQ                                             

  TRIG        Initial EMTF algo with GE1/1             Dec 31 
              integrated into firmware                        

  TRIG        Initial EMTF displaced muon algo         Dec 31 
              implemented into firmware                       

  TRIG        Prepare BMTF Kalman Filter for Run       Dec 31 
              3 physics and fit in latency                    

  TRIG        Prepare version of BMTF algo to          Jun 30 
              interface with new DT electronics               
              in Sector 12                                    

  TRIG        Calo Trig: New calibration               Dec 31 
              procedure/constants to be installed             
              in firmware                                     

  TRIG        Calo Trig: Together with ECAL group      Dec 31 
              pull additional spare fibers                    
              between ECAL/L1 Calo and test them              

  TRIG        Calo Trig: Update the software and       Dec 31 
              firmware for multiple BX                        

  TRIG        Calo Trig: Upgrade embedded Linux        Dec 31 
              on CTP7                                         

  TRIG        Convert trigger rate monitoring          Aug 31 
              code to Python3                                 

  TRIG        Test trigger rate monitoring code        Dec 31 
              using ODMS as data source                       
  ----------------------------------------------------------------------

  : Trigger 2020 Milestones

